package com.example.reto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.reto.exceptions.AdmissionDateException;
import com.example.reto.exceptions.ExistIdentficationTypeAndNumberException;
import com.example.reto.exceptions.IdEmployeeNullException;
import com.example.reto.exceptions.NotExistsEmployeeException;
import com.example.reto.model.Area;
import com.example.reto.model.Country;
import com.example.reto.model.Employee;
import com.example.reto.model.IdentificationType;
import com.example.reto.model.Traceability;
import com.example.reto.repositories.EmployeeRepository;
import com.example.reto.services.EmployeeService;

@RunWith(SpringRunner.class)
@SpringBootTest
class RetoApplicationTests {
	
	@InjectMocks
	private EmployeeService employeeService;
	
	@Mock
	private EmployeeRepository employeeRepository;
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * crear un empleado con un fecha de ingrese menor a un mes que la fecha actual
	 */
	@Test
	public void createEmployeeAdmissionDateException() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -32);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(null);
		
		AdmissionDateException admissionDateException = Assertions.assertThrows(AdmissionDateException.class, 
				() -> employeeService.createEmployee(employee));
		Assertions.assertEquals("La fecha de ingreso no puede ser menor a un mes o superior a la fecha actual", admissionDateException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * crear un empleado con un fecha de ingrese mayor a la fecha actual
	 */
	@Test
	public void createEmployeeAdmissionDateException2() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, +1);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(null);
		
		AdmissionDateException admissionDateException = Assertions.assertThrows(AdmissionDateException.class, 
				() -> employeeService.createEmployee(employee));
		Assertions.assertEquals("La fecha de ingreso no puede ser menor a un mes o superior a la fecha actual", admissionDateException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * crear un empleado con un tipo de identificación y número de identificación que ya se encuentra
	 * registrado en la base de datos
	 */
	@Test
	public void createEmployeeIdentificationTypeAndNumberExist() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(new Employee());
		
		ExistIdentficationTypeAndNumberException identificationTypeAndNumberException = Assertions.assertThrows(ExistIdentficationTypeAndNumberException.class, 
				() -> employeeService.createEmployee(employee));
		
		Assertions.assertEquals("El tipo de identificación y el número de identificación que intenta ingresar ya se encuentra registrado", identificationTypeAndNumberException.getMessage());
	
		Mockito.verify(employeeRepository).findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber());
	}
	
	/**
	 * Prueba que verifica si realmente se creó el empleado
	 */
	@Test
	public void createEmployee() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Mockito.when(employeeRepository.findByEmailContainingOrderByEmailAsc(""))
		.thenReturn(new ArrayList<Employee>());
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(null);
		Mockito.when(employeeRepository.save(employee))
		.thenReturn(employee);
		
		Employee employeeCreated = employeeService.createEmployee(employee);
		
		Assertions.assertEquals("ANDRES", employeeCreated.getFirstName());
	
		Mockito.verify(employeeRepository).save(employee);
	}
	
	/**
	 * Prueba que verifica si realmente se creó el empleado con el correo adecuado
	 */
	@Test
	public void createEmployeeEmail() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gnzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Employee employee2 = new Employee();
		employee2.setFirstName("ANDRES");
		employee2.setOtherNames("JUAN");
		employee2.setFirstLastName("GONZALEZ");
		employee2.setSecondLastName("RINCON");
		Calendar date2 = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee2.setAdmissionDate(date2.getTime());
		employee2.setState("ACTIVO");
		employee2.setEmail("andres.gonzalez@cidenet.com.co");
		employee2.setIdentificationNumber("9259asd");
		employee2.setIdentificationType(identificationType);
		employee2.setArea(area);
		employee2.setCountry(country);
		
		List<Employee> employees = new ArrayList<Employee>();
		employees.add(employee2);
		
		Mockito.when(employeeRepository.findByEmailContainingOrderByEmailAsc(""))
		.thenReturn(employees);
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(null);
		Mockito.when(employeeRepository.save(employee))
		.thenReturn(employee);
		
		Employee employeeCreated = employeeService.createEmployee(employee);
		
		System.out.println(employeeCreated.getSecondLastName());
		Assertions.assertEquals("andres.gonzalez.1@cidenet.com.co", employeeCreated.getEmail());
	
		Mockito.verify(employeeRepository).save(employee);
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * actualizar un empleado con un fecha de ingrese menor a un mes que la fecha actual
	 */
	@Test
	public void updateEmployeeAdmissionDate() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -32);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		AdmissionDateException admissionDateException = Assertions.assertThrows(AdmissionDateException.class, 
				() -> employeeService.updateEmployee(employee));
		Assertions.assertEquals("La fecha de ingreso no puede ser menor a un mes o superior a la fecha actual", admissionDateException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * actualizar un empleado con un fecha de ingrese mayor a la fecha actual
	 */
	@Test
	public void updateEmployeeAdmissionDate2() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, +2);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		AdmissionDateException admissionDateException = Assertions.assertThrows(AdmissionDateException.class, 
				() -> employeeService.updateEmployee(employee));
		Assertions.assertEquals("La fecha de ingreso no puede ser menor a un mes o superior a la fecha actual", admissionDateException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * actualizar un empleado nulo
	 */
	@Test
	public void updateEmployeeIdNullException() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, +2);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		IdEmployeeNullException idEmployeeNullException = Assertions.assertThrows(IdEmployeeNullException.class, 
				() -> employeeService.updateEmployee(null));
		Assertions.assertEquals("El id y el empleado no puede ser nulo", idEmployeeNullException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * actualizar un empleado con email nulo
	 */
	@Test
	public void updateEmployeeIdNullException2() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, +2);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail(null);
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		IdEmployeeNullException idEmployeeNullException = Assertions.assertThrows(IdEmployeeNullException.class, 
				() -> employeeService.updateEmployee(employee));
		Assertions.assertEquals("El id y el empleado no puede ser nulo", idEmployeeNullException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * actualizar un empleado que no existe
	 */
	@Test
	public void updateEmployeeNotExistEmployee() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Mockito.when(employeeRepository.findByEmail(employee.getEmail()))
		.thenReturn(null);
		
		NotExistsEmployeeException notExistEmployeeException = Assertions.assertThrows(NotExistsEmployeeException.class, 
				() -> employeeService.updateEmployee(employee));
		Assertions.assertEquals("El empleado con email "+employee.getEmail()+" no existe en la base de datos", notExistEmployeeException.getMessage());
	}
	
	/**
	 * Prueba que verifica si realmente se lanza la excepción cuando se intenta 
	 * actualizar un empleado donde se actualizó el el número de identificación
	 * y con ese nuevo numero de identificacion y numero de identificacion ya exite un empleado
	 */
	@Test
	public void updateEmployeeExistIdentificationTypeAndNumberException() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		Employee employee2 = new Employee();
		employee2.setFirstName("ANDRES");
		employee2.setOtherNames("FELIPE");
		employee2.setFirstLastName("GONZALEZ");
		employee2.setSecondLastName("CARDONA");
		Calendar date2 = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee2.setAdmissionDate(date2.getTime());
		employee2.setState("ACTIVO");
		employee2.setEmail("andres.gonzalez@cidenet.com.co");
		employee2.setIdentificationNumber("asdasdv");
		employee2.setIdentificationType(identificationType);
		employee2.setArea(area);
		employee2.setCountry(country);
		
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(new Employee());
		Mockito.when(employeeRepository.findByEmail(employee.getEmail()))
		.thenReturn(employee2);
		
		ExistIdentficationTypeAndNumberException identificationTypeAndNumberException = Assertions.assertThrows(ExistIdentficationTypeAndNumberException.class, 
				() -> employeeService.updateEmployee(employee));
		
		Assertions.assertEquals("El tipo de identificación y el número de identificación que intenta ingresar ya se encuentra registrado", identificationTypeAndNumberException.getMessage());
	}
	
	/**
	 * Se verifica si realmente se actualiza el empleado
	 */
	@Test
	public void updateEmployee() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		List<Traceability> traceabilities = new ArrayList<Traceability>();
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		employee.setTraceabilities(traceabilities);
		
		
		Employee employee2 = new Employee();
		employee2.setFirstName("ANDRES");
		employee2.setOtherNames("FELIPE");
		employee2.setFirstLastName("GONZALEZ");
		employee2.setSecondLastName("RINCON");
		Calendar date2 = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -15);
		employee2.setAdmissionDate(date2.getTime());
		employee2.setState("ACTIVO");
		employee2.setEmail("andres.gonzalez@cidenet.com.co");
		employee2.setIdentificationNumber("asdasdv");
		employee2.setIdentificationType(identificationType);
		employee2.setArea(area);
		employee2.setCountry(country);
		employee2.setTraceabilities(traceabilities);
		
		Mockito.when(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(identificationType.getId(), employee.getIdentificationNumber()))
		.thenReturn(null);
		Mockito.when(employeeRepository.findByEmail(employee.getEmail()))
		.thenReturn(employee2);
		Mockito.when(employeeRepository.save(employee))
		.thenReturn(employee2);
		
		Employee employeeAux = employeeService.updateEmployee(employee);
		
		Assertions.assertEquals("RINCON", employeeAux.getSecondLastName());
	}
	
	@Test
	void contextLoads() {
	}

}
