package com.example.reto.tests;

import java.util.Calendar;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.reto.exceptions.AdmissionDateException;
import com.example.reto.model.Area;
import com.example.reto.model.Country;
import com.example.reto.model.Employee;
import com.example.reto.model.IdentificationType;
import com.example.reto.services.EmployeeService;

@ExtendWith(SpringExtension.class)
public class EmployeeTests {
	
	@InjectMocks
	private EmployeeService employeeService;
	
	@Test
	public void createEmployeeAdmissionDateException() {
		IdentificationType identificationType = new IdentificationType();
		identificationType.setId(new Long(1));
		identificationType.setName("Cédula de Ciudadanía");
		
		Area area = new Area();
		area.setId(new Long(1));
		area.setName("Talento Humano");
		
		Country country = new Country();
		country.setId(new Long(1));
		country.setName("Colombia");
		
		Employee employee = new Employee();
		employee.setFirstName("ANDRES");
		employee.setOtherNames("FELIPE");
		employee.setFirstLastName("GONZALEZ");
		employee.setSecondLastName("CARDONA");
		Calendar date = Calendar.getInstance();
		date.add(Calendar.DAY_OF_YEAR, -32);
		employee.setAdmissionDate(date.getTime());
		employee.setState("ACTIVO");
		employee.setEmail("andres.gonzalez@cidenet.com.co");
		employee.setIdentificationNumber("asdf95");
		employee.setIdentificationType(identificationType);
		employee.setArea(area);
		employee.setCountry(country);
		
		AdmissionDateException admissionDateException = Assertions.assertThrows(AdmissionDateException.class, 
				() -> employeeService.createEmployee(employee));
		Assertions.assertEquals("La fecha de ingreso no puede ser menor a un mes o superior a la fecha actual", admissionDateException.getMessage());
	}

}
