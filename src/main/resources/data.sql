INSERT INTO areas (id, name) VALUES (1, 'Administración');
INSERT INTO areas (id, name) VALUES (2, 'Financiera');
INSERT INTO areas (id, name) VALUES (3, 'Compras');
INSERT INTO areas (id, name) VALUES (4, 'Infraestructura');
INSERT INTO areas (id, name) VALUES (5, 'Operación');
INSERT INTO areas (id, name) VALUES (6, 'Talento Humano');
INSERT INTO areas (id, name) VALUES (7, 'Servicios Varios');

INSERT INTO countries (id, name) VALUES (1, 'Colombia');
INSERT INTO countries (id, name) VALUES (2, 'Estados Unidos');

INSERT INTO identifications_type (id, name) VALUES (1, 'Cédula de Ciudadanía');
INSERT INTO identifications_type (id, name) VALUES (2, 'Cédula de Extranjería');
INSERT INTO identifications_type (id, name) VALUES (3, 'Pasaporte');
INSERT INTO identifications_type (id, name) VALUES (4, 'Permiso Especial');

INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('andres.gonzalez@cidenet.com.co', 'ANDRES', 'FELIPE', 'GONZALEZ', 'CARDONA', 1, '12345', 1, 1, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('felipe.granada@cidenet.com.co', 'FELIPE', '', 'GRANADA', 'TORRES', 1, '123456', 1, 2, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('juan.cordoba@cidenet.com.co', 'JUAN', 'FELIPE', 'CORDOBA', 'CORDOBA', 1, '12348', 1, 3, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('maria.restrepo@cidenet.com.co', 'MARIA', 'NUVIA', 'RESTREPO', 'SANCHEZ', 1, '12341', 1, 4, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('julian.sanchez@cidenet.com.co', 'JULIAN', '', 'SANCHEZ', 'MARIN', 1, '12342', 2, 5, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('orlando.castro@cidenet.com.co', 'ORLANDO', 'JOSE', 'CASTRO', 'LOPEZ', 1, '12343', 2, 6, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('oscar.valencia@cidenet.com.co', 'OSCAR', '', 'VALENCIA', 'VALENCIA', 1, '12344', 1, 7, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('andrea.murillo@cidenet.com.co', 'ANDREA', '', 'MURILLO', 'SANDOBAL', 1, '12346', 2, 1, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('luisa.granada@cidenet.com.co', 'LUISA', 'MARIA', 'GRANADA', 'ORDOÑEZ', 1, '12347', 1, 2, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('paula.rincon@cidenet.com.co', 'PAULA', 'ANDREA', 'RINCON', 'SANCHEZ', 1, '12349', 1, 3, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('carolina.cardona@cidenet.com.co', 'CAROLINA', '', 'CARDONA', 'POTES', 1, '12315', 1, 4, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('eliana.lopez@cidenet.com.co', 'ELIANA', '', 'LOPEZ', 'JARAMILLO', 1, '12325', 2, 5, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('pablo.osorio@cidenet.com.co', 'PABLO', '', 'OSORIO', 'GRANADA', 1, '12335', 1, 6, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('juan.cordoba.1@cidenet.com.co', 'JUAN', 'DAVID', 'CORDOBA', 'MARIN', 1, '12355', 1, 7, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('giovany.gonzalez@cidenet.com.co', 'GIOVANY', '', 'GONZALEZ', 'SANCHEZ', 1, '12365', 1, 1, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('luis.delacalle@cidenet.com.co', 'LUIS', 'ALBERTO', 'DE LA CALLE', 'GOMEZ', 1, '12375', 1, 2, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('natalia.osorio@cidenet.com.co', 'NATALIA', '', 'OSORIO', 'CARDONA', 1, '12385', 2, 3, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('andres.gonzalez.1@cidenet.com.co', 'ANDRES', '', 'GONZALEZ', 'RESTREPO', 1, '12395', 1, 4, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('catalina.gomez@cidenet.com.co', 'CATALINA', '', 'GOMEZ', 'SANCHEZ', 1, '12145', 1, 5, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('alejandra.renteria@cidenet.com.co', 'ALEJANDRA', '', 'RENTERIA', 'RENTERIA', 1, '12245', 1, 6, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('alejandro.ramos@cidenet.com.co', 'ALEJANDRO', '', 'RAMOS', 'OSORIO', 1, '12445', 2, 7, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('carlos.grajales@cidenet.com.co', 'CARLOS', 'EDUARDO', 'GRAJALES', 'MURILLO', 1, '12545', 2, 1, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('andres.gonzalez.2@cidenet.com.co', 'ANDRES', '', 'GONZALEZ', 'SEPULVEDA', 1, '12645', 2, 2, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('felipe.granada.1@cidenet.com.co', 'FELIPE', '', 'GRANADA', 'ORDOÑEZ', 1, '12745', 2, 3, 'ACTIVO', NOW());
INSERT INTO employees (email, first_name, other_names, first_last_name, second_last_name, identification_type_id, identification_number, country_id, area_id, state, admission_date) VALUES ('diana.castro@cidenet.com.co', 'DIANA', 'ISABEL', 'CASTRO', 'CASTRO', 1, '12845', 2, 4, 'ACTIVO', NOW());




INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (1, 'REGISTRO', NOW(), 'andres.gonzalez@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (2, 'REGISTRO', NOW(), 'felipe.granada@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (3, 'REGISTRO', NOW(), 'juan.cordoba@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (4, 'REGISTRO', NOW(), 'maria.restrepo@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (5, 'REGISTRO', NOW(), 'julian.sanchez@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (6, 'REGISTRO', NOW(), 'orlando.castro@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (7, 'REGISTRO', NOW(), 'oscar.valencia@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (8, 'REGISTRO', NOW(), 'andrea.murillo@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (9, 'REGISTRO', NOW(), 'luisa.granada@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (10, 'REGISTRO', NOW(), 'paula.rincon@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (11, 'REGISTRO', NOW(), 'carolina.cardona@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (12, 'REGISTRO', NOW(), 'eliana.lopez@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (13, 'REGISTRO', NOW(), 'pablo.osorio@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (14, 'REGISTRO', NOW(), 'juan.cordoba.1@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (15, 'REGISTRO', NOW(), 'giovany.gonzalez@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (16, 'REGISTRO', NOW(), 'luis.delacalle@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (17, 'REGISTRO', NOW(), 'natalia.osorio@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (18, 'REGISTRO', NOW(), 'andres.gonzalez.1@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (19, 'REGISTRO', NOW(), 'catalina.gomez@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (21, 'REGISTRO', NOW(), 'alejandra.renteria@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (22, 'REGISTRO', NOW(), 'alejandro.ramos@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (23, 'REGISTRO', NOW(), 'carlos.grajales@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (24, 'REGISTRO', NOW(), 'andres.gonzalez.2@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (25, 'REGISTRO', NOW(), 'felipe.granada.1@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (26, 'REGISTRO', NOW(), 'diana.castro@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (27, 'FECHA_EDICION', NOW(), 'diana.castro@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (28, 'FECHA_EDICION', NOW(), 'felipe.granada.1@cidenet.com.co');
INSERT INTO traceabilities (id, type, registration_date, employee_email) VALUES (29, 'FECHA_EDICION', NOW(), 'andres.gonzalez.2@cidenet.com.co');
