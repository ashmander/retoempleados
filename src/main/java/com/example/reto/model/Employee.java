package com.example.reto.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "employees", uniqueConstraints = {@UniqueConstraint(columnNames = {"identification_type_id", "identification_number"})})
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Indica el id del empleado que también es el correo del empelado
	 */
	@Id
	@Email(message = "El email debe tener una estructura válida")
	@Size(max = 300, message = "El email no debe tener más de 300 caracteres")
	@Column(name = "email")
	private String email;
	
	/**
	 * Indica el primer apellido del empleado
	 */
	@Pattern(regexp = "^[ A-Z]*$", message = "El primer apellido debe ser A-Z, sin acentos y sin ñ")
	@NotEmpty(message = "El primer apellido no debe estar vacío")
	@NotNull(message = "El primer apellido no debe estar nulo")
	@Size(max = 20, message = "El primer apellido no debe tener más de 20 caracteres")
	@Column(name = "first_last_name")
	private String firstLastName;
	
	/**
	 * Indica el segundo apellido del empleado
	 */
	@Pattern(regexp = "^[ A-Z]*$", message = "El segundo apellido debe ser A-Z, sin acentos y sin ñ")
	@NotEmpty(message = "El segundo apellido no debe estar vacío")
	@NotNull(message = "El segundo nombre no debe estar nulo")
	@Size(max = 20, message = "El segundo apellido no debe tener más de 20 caracteres")
	@Column(name = "second_last_name")
	private String secondLastName;
	
	/**
	 * Indica el primer nombre del empleado
	 */
	@Pattern(regexp = "^[ A-Z]*$", message = "El primer nombre debe ser A-Z, sin acentos y sin ñ")
	@NotEmpty(message = "El primer nombre no debe estar vacío")
	@NotNull(message = "El primer nombre no debe estar nulo")
	@Size(max = 20, message = "El primer nombre no debe tener más de 20 caracteres")
	@Column(name = "first_name")
	private String firstName;
	
	/**
	 * Indica los demás nombres del empleado
	 */
	@Pattern(regexp = "^[A-Z ]*$", message = "Los demás nombres deben ser A-Z, sin acento y sin ñ")
	@Size(max = 50, message = "Los demás nombre deben tener un tamaño máximo de 50 caracteres")
	@Column(name = "other_names")
	private String otherNames;
	
	/**
	 * Indica el país en el cual el empleado trabajará
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull(message = "El país no debe ser nulo")
	@JoinColumn(name = "country_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Country country;
	
	/**
	 * Indica el tipo de identificación que tiene el empleado
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull(message = "El tipo de identificación no debe ser nulo")
	@JoinColumn(name = "identification_type_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private IdentificationType identificationType;
	
	/**
	 * Indica el número de identificación del empleado
	 */
	@Pattern(regexp = "^[A-Za-z0-9]+$", message = "El número de identificación debe ser A-Z, a-z, 0-9, sin acentos y sin ñ")
	@Size(max = 20, message = "El número de identificación debe tener máximo 20 caracteres")
	@NotEmpty(message = "El número de identificación no debe estar vacío")
	@NotNull(message = "El número de identificación no debe estar nulo")
	@Column(name = "identification_number")
	private String identificationNumber;
	
	/**
	 * Indica la fecha de ingreso del empleado
	 */
	@Temporal(TemporalType.DATE)
	@NotNull(message = "La fecha de ingreso no debe estar nulo")
	@Column(name = "admission_date")
	private Date admissionDate;
	
	/**
	 * Indica el área al cual pertenece el empleado
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull(message = "El área no debe estar nulo")
	@JoinColumn(name = "area_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Area area;

	/**
	 * Indica el estado del empleado, en el momento siempre ACTIVO
	 */
	@Column(name = "state")
	private String state;
	
	/**
	 * Indica la trazabilidad que se le hace al registro y actualización de empleados
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "employee_email")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private List<Traceability> traceabilities;

	public String getFirstLastName() {
		return firstLastName;
	}

	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getOtherNames() {
		return otherNames;
	}

	public void setOtherNames(String otherNames) {
		this.otherNames = otherNames;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public IdentificationType getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(IdentificationType identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public List<Traceability> getTraceabilities() {
		return traceabilities;
	}

	public void setTraceabilities(List<Traceability> traceabilities) {
		this.traceabilities = traceabilities;
	}
}
