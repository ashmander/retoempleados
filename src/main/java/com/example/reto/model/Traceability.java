package com.example.reto.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Table(name = "traceabilities")
public class Traceability implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Indica el id de la trasabilidad a la hora de registrar o actualizar un empleado
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Indica el tipo de registro que se hizo: REGISTRADO o ACTUALIZADO
	 */
	private String type;
	
	/**
	 * Indica la fecha y hora en la que se hizo el registro o la actualización
	 */
	@DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	@Column(name = "registration_date")
	private LocalDateTime registrationDate;
	
	public Traceability () {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public LocalDateTime getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(LocalDateTime registrationDate) {
		this.registrationDate = registrationDate;
	}
}
