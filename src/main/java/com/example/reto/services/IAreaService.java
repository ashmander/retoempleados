package com.example.reto.services;

import java.util.List;

import com.example.reto.model.Area;

public interface IAreaService {

	public List<Area> findAll();
}
