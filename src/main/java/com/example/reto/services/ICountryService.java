package com.example.reto.services;

import java.util.List;

import com.example.reto.model.Country;

public interface ICountryService {

	public List<Country> findAll();
}
