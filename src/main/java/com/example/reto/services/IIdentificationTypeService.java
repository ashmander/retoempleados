package com.example.reto.services;
import java.util.List;

import com.example.reto.model.IdentificationType;

public interface IIdentificationTypeService {

	public List<IdentificationType> findAll();
}
