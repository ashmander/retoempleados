package com.example.reto.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.reto.exceptions.AdmissionDateException;
import com.example.reto.exceptions.IdEmployeeNullException;
import com.example.reto.exceptions.NotExistsEmployeeException;
import com.example.reto.model.Employee;

public interface IEmployeeService {

	public Employee createEmployee(Employee employee) throws AdmissionDateException;
	
	public void deleteEmployee(String email) throws IdEmployeeNullException, NotExistsEmployeeException;
	
	public Employee updateEmployee(Employee employee) throws AdmissionDateException, NotExistsEmployeeException, IdEmployeeNullException;
	
	public Page<Employee> findAllEmployees(Pageable page);
	
	public Employee getEmployee(String email) throws IdEmployeeNullException, NotExistsEmployeeException;
	
	public Page<Employee> filterEmployees(Employee employee, Pageable page);
}
