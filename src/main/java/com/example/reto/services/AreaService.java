package com.example.reto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reto.model.Area;
import com.example.reto.repositories.AreaRepository;

@Service
public class AreaService implements IAreaService {

	@Autowired
	private AreaRepository areaRepository;
	
	/**
	 * Método para obtener todas las áreas de la base de datos
	 */
	@Override
	public List<Area> findAll() {
		return areaRepository.findAll();
	}

}
