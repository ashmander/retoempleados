package com.example.reto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reto.model.Country;
import com.example.reto.repositories.CountryRepository;

@Service
public class CountryService implements ICountryService {

	@Autowired
	private CountryRepository countryRepository;

	/**
	 * Método para obtener todos los paises de la base de datos
	 */
	@Override
	public List<Country> findAll() {
		return countryRepository.findAll();
	}
}
