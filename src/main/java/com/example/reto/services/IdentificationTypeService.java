package com.example.reto.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.reto.model.IdentificationType;
import com.example.reto.repositories.IdentificationTypeRepository;

@Service
public class IdentificationTypeService implements IIdentificationTypeService {

	@Autowired
	private IdentificationTypeRepository identificationTypeRepository;

	/**
	 * Método para obtener todos los tipos de identificación
	 */
	@Override
	public List<IdentificationType> findAll() {
		return identificationTypeRepository.findAll();
	}
}
