package com.example.reto.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.reto.exceptions.AdmissionDateException;
import com.example.reto.exceptions.ExistIdentficationTypeAndNumberException;
import com.example.reto.exceptions.IdEmployeeNullException;
import com.example.reto.exceptions.NotExistsEmployeeException;
import com.example.reto.model.Employee;
import com.example.reto.model.Traceability;
import com.example.reto.repositories.EmployeeRepository;

@Service
public class EmployeeService implements IEmployeeService {
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	/**
	 * Métodos para crear un empleado
	 */
	@Override
	public Employee createEmployee(Employee employee)  throws AdmissionDateException {
		//Se verifica si existe un empleado con el mismo tipo de identificación y el mismo número de identificación
		existIdentificationNumberAndType(employee);
		//Se verifica si la fecha de ingreso es mayor a la actual o menor a un mes que la actual
		if(validateAdmissionDate(employee.getAdmissionDate())) {
			//se genera el email
			employee.setEmail(createEmail(employee.getFirstName(), employee.getFirstLastName()));
			employee.setState("ACTIVO");
			Traceability traceability = new Traceability();
			traceability.setType("FECHA_REGISTRO");
			LocalDateTime dateNow = LocalDateTime.now();
			traceability.setRegistrationDate(dateNow);
			List<Traceability> traceabilities = new ArrayList<Traceability>();
			traceabilities.add(traceability);
			employee.setTraceabilities(traceabilities);
			System.out.println(employee.getEmail());
			System.out.println(employee.getFirstName());
			return employeeRepository.save(employee);
		} else {
			throw new AdmissionDateException();
		}
	}

	/**
	 * Método para borrar un empleado
	 */
	@Override
	public void deleteEmployee(String email) throws IdEmployeeNullException, NotExistsEmployeeException {
		if(email == null) {
			throw new IdEmployeeNullException();
		}
		Employee employee = employeeRepository.findById(email).orElse(null);
		if(employee == null) {
			throw new NotExistsEmployeeException(email);
		}
		employeeRepository.deleteById(email);
	}

	/**
	 * Método para actualizar un empleado
	 */
	@Override
	public Employee updateEmployee(Employee employee) throws AdmissionDateException, NotExistsEmployeeException, IdEmployeeNullException {
		if(employee == null || employee.getEmail() == null) {
			throw new IdEmployeeNullException();
		}
		//Se verifica si la fecha de ingreso es mayor a la actual o menor a un mes que la actual
		if(validateAdmissionDate(employee.getAdmissionDate())) {
			Employee employeeToUpdate = employeeRepository.findByEmail(employee.getEmail());
			if(employeeToUpdate != null) {
				String emailCreated = "";
				//Se valida si se modificó el primer nombre o el primer apellido para poder volver a generar el correo
				if(!employeeToUpdate.getFirstName().equals(employee.getFirstName()) || !employeeToUpdate.getFirstLastName().equals(employee.getFirstLastName())) {
					employeeToUpdate.setFirstName(employee.getFirstName());
					employeeToUpdate.setFirstLastName(employee.getFirstLastName());
					emailCreated = createEmail(employee.getFirstName(), employee.getFirstLastName());
					employeeRepository.deleteById(employeeToUpdate.getEmail());
					employeeToUpdate.setEmail(emailCreated);			
				}
				//Se verifica si el empleado modifico el tipo de identificación o el número de identificación para poder validar si ya existe un empleado cone esos datos
				if(employeeToUpdate.getIdentificationType().getId().compareTo(employee.getIdentificationType().getId()) != 0 || !employeeToUpdate.getIdentificationNumber().equals(employee.getIdentificationNumber())) {
					existIdentificationNumberAndType(employee);
				}
				employeeToUpdate.setArea(employee.getArea());
				employeeToUpdate.setCountry(employee.getCountry());
				employeeToUpdate.setOtherNames(employee.getOtherNames());
				employeeToUpdate.setSecondLastName(employee.getSecondLastName());
				employeeToUpdate.setAdmissionDate(employee.getAdmissionDate());
				employeeToUpdate.setIdentificationType(employee.getIdentificationType());
				employeeToUpdate.setIdentificationNumber(employee.getIdentificationNumber());
				Traceability traceability = new Traceability();
				traceability.setType("FECHA_EDICION");
				LocalDateTime dateNow = LocalDateTime.now();
				traceability.setRegistrationDate(dateNow);
				employeeToUpdate.getTraceabilities().add(traceability);
				return employeeRepository.save(employeeToUpdate);							
			} else {
				throw new NotExistsEmployeeException(employee.getEmail());
			}
		} else {
			throw new AdmissionDateException();
		}
	}
	
	/**
	 * Método para verificar si ya existe un empleado con el tipo de identificación y el número de identificación dado
	 * @param employee: Empleadoo a validar
	 */
	private void existIdentificationNumberAndType(Employee employee) {
		if(employeeRepository.findByIdentificationTypeIdAndIdentificationNumber(employee.getIdentificationType().getId(), employee.getIdentificationNumber()) != null) {
			throw new ExistIdentficationTypeAndNumberException();
		}
	}
	
	/**
	 * Se válida si la fecha de ingreso del empleado es menor a un mes o mayor a la fecha actual
	 */
	private boolean validateAdmissionDate(Date date) {
		Calendar dateToValidate = Calendar.getInstance();
		dateToValidate.setTime(date);
		Calendar currentDate = Calendar.getInstance();
		Calendar minDate = Calendar.getInstance();
		minDate.add(Calendar.DAY_OF_YEAR, -30);
		if(dateToValidate.get(Calendar.DAY_OF_YEAR) <= currentDate.get(Calendar.DAY_OF_YEAR) && dateToValidate.get(Calendar.DAY_OF_YEAR) >= minDate.get(Calendar.DAY_OF_YEAR)) {
			return true;
		} 
		return false;
	}

	/**
	 * Método para generar el correo del empleado
	 * @param firstName: Primer nombre del empleado
	 * @param firstLastName: Primer apellido del empleado
	 * @return El corroe del empleado
	 */
	public String createEmail(String firstName, String firstLastName) {
		// Se prepara el primer nombre y primer apellido
		System.out.println(firstName + "  " + firstLastName);
		String firstPart = firstName.toLowerCase().trim().replace(" ", "") + "."
				+ firstLastName.toLowerCase().trim().replace(" ", "");
		String email = "";
		List<Employee> employees = employeeRepository.findByEmailContainingOrderByEmailAsc(firstPart);
		if (employees.size() > 0) {
			int lastNumber = 0;
			if (employees.size() == 1) {
				if (employees.get(0).getEmail().split("\\.").length != 4) {
					int aux = Integer.parseInt(employees.get(0).getEmail().split("\\.")[2].split("@")[0]);
					if (lastNumber < aux) {
						lastNumber = aux;
					}
				} else {
					email = firstPart + ".1@cidenet.com.co";
				}
			} else {
				for (int i = 0; i < employees.size(); i++) {
					if (employees.get(i).getEmail().split("\\.").length != 4) {
						int aux = Integer.parseInt(employees.get(i).getEmail().split("\\.")[2].split("@")[0]);
						if (lastNumber < aux) {
							lastNumber = aux;
						}
					}

				}
			}
			lastNumber++;
			email = firstPart + "." + lastNumber + "@cidenet.com.co";

		} else {
			email = firstPart + "@cidenet.com.co";
		}
		return email;
	}

	/**
	 * Método para obtener una lista de empleados, se obtienen de 10 en 10
	 */
	@Override
	public Page<Employee> findAllEmployees(Pageable page) {
		return employeeRepository.findAll(page);
	}

	/**
	 * Método para obtener un empleado por su correo
	 */
	@Override
	public Employee getEmployee(String email) throws IdEmployeeNullException, NotExistsEmployeeException {
		if(email == null) {
			throw new IdEmployeeNullException();
		}
		Employee employee = employeeRepository.findById(email).orElse(null);
		if(employee == null) {
			throw new NotExistsEmployeeException(email);
		}
		return employee;
	}

	/**
	 * Método para filtrar los empleados
	 */
	@Override
	public Page<Employee> filterEmployees(Employee employee, Pageable page) {
		Long identificationTypeId = employee.getIdentificationType() != null ? employee.getIdentificationType().getId().compareTo(new Long(-1)) != 0 ? employee.getIdentificationType().getId() : null : null;
		Long countryId = employee.getCountry() != null ? employee.getCountry().getId().compareTo(new Long(-1)) != 0 ? employee.getCountry().getId() : null : null;
		String firstName = employee.getFirstName() != null ? !employee.getFirstName().equals("") ? employee.getFirstName().toUpperCase() : null : null;
		String otherNames = employee.getOtherNames() != null ? !employee.getOtherNames().equals("") ? employee.getOtherNames().toUpperCase() : null : null;
		String firstLastName = employee.getFirstLastName() != null ? !employee.getFirstLastName().equals("") ? employee.getFirstLastName().toUpperCase() : null : null;
		String secondLastName = employee.getSecondLastName() != null ? !employee.getSecondLastName().equals("") ? employee.getSecondLastName().toUpperCase() : null : null;
		String identificationNumber = employee.getIdentificationNumber() != null ? !employee.getIdentificationNumber().equals("") ? employee.getIdentificationNumber() : null : null;
		String state = employee.getState() != null ? !employee.getState().equals("") ? employee.getState().toUpperCase() : null : null;
		String email = employee.getEmail() != null ? !employee.getEmail().equals("") ? employee.getEmail().toLowerCase() : null : null;
		return employeeRepository.findByEmailContainingAndIdentificationTypeId(
				email, 
				identificationTypeId, 
				firstName,
				otherNames,
				firstLastName,
				secondLastName,
				identificationNumber,
				state,
				countryId,
				page);
	}
}
