package com.example.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.reto.model.Area;
import com.example.reto.services.IAreaService;

@RestController
@RequestMapping("/areas")
@CrossOrigin("*")
public class AreaController {

	@Autowired
	private IAreaService areaService;
	
	/**
	 * Método para obtener la lista de áreas a las que puede pertener un empleado
	 * @return áreas
	 */
	@GetMapping("")
	public List<Area> getAll() {
		return areaService.findAll();
	}
}
