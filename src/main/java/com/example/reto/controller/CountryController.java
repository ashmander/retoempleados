package com.example.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.reto.model.Country;
import com.example.reto.services.ICountryService;

@RestController
@RequestMapping("/countries")
@CrossOrigin("*")
public class CountryController {

	@Autowired
	private ICountryService countryService;
	
	/**
	 * Método para obtener la lista de paises en los que puede trabajar un empleado
	 * @return paises
	 */
	@GetMapping("")
	public List<Country> getAll() {
		return countryService.findAll();
	}
}
