package com.example.reto.controller;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.reto.exceptions.AdmissionDateException;
import com.example.reto.exceptions.ExistIdentficationTypeAndNumberException;
import com.example.reto.exceptions.IdEmployeeNullException;
import com.example.reto.exceptions.NotExistsEmployeeException;
import com.example.reto.model.Employee;
import com.example.reto.services.IEmployeeService;

@RestController
@RequestMapping("/employees")
@CrossOrigin("*")
public class EmployeeController {

	@Autowired
	private IEmployeeService employeeService;
	
	/**
	 * Método para obtener la lista de empleados, se obtendran de 10 en 10
	 * @param número de la página que se quiere obtener
	 * @return lista de empleados
	 */
	@GetMapping("/page/{page}")
	public Page<Employee> getEmployees(@PathVariable Integer page) {
		return employeeService.findAllEmployees(PageRequest.of(page, 10));
	}
	
	/**
	 * Método para crear un empleado
	 * @param employee: Empleado que se quiere crear
	 * @param result: Validador de errores
	 * @return Empleado creado o mensaje de error
	 */
	@PostMapping("/create")
	public ResponseEntity<?> createEmployee(@Validated @RequestBody Employee employee, BindingResult result) {
		Map<String, Object> response = new HashMap<String, Object>();
		if(result.hasErrors()) {
			String errors = "";
			for(FieldError error : result.getFieldErrors()) {
				errors += error.getDefaultMessage() + "\n";
			}
			response.put("error", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			return new ResponseEntity<Employee>(employeeService.createEmployee(employee), HttpStatus.CREATED);
		} catch (ExistIdentficationTypeAndNumberException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		} catch (AdmissionDateException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			response.put("error", "Ha ocurrido un error al intentar crear el empleado");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Método para actualizar un empleado
	 * @param employee: Empleado a actualizar
	 * @param result: Validador de erroes
	 * @return Empleado actualizado o mensaje de error
	 */
	@PutMapping("/update")
	public ResponseEntity<?> updateEmployee(@Validated @RequestBody Employee employee, BindingResult result) {
		Map<String, Object> response = new HashMap<String, Object>();
		if(result.hasErrors()) {
			List<String> errors = new ArrayList<String>();
			for(FieldError error : result.getFieldErrors()) {
				errors.add(error.getDefaultMessage());
			}
			response.put("error", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		try {
			return new ResponseEntity<Employee>(employeeService.updateEmployee(employee), HttpStatus.CREATED);
		} catch (AdmissionDateException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		} catch (IdEmployeeNullException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		} catch (NotExistsEmployeeException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			response.put("error", "Ha ocurrido un error al intentar actualizar el empleado");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Método para borrar un empleado
	 * @param email: Correo del empleado que se quiere borrar
	 * @return Mensaje confirmando que se ha borrado el empleado o mensaje de error
	 */
	@DeleteMapping("/delete/{email}")
	public ResponseEntity<?> deleteEmployee(@PathVariable String email) {
		Map<String, String> response = new HashMap<String, String>();
		try {
			employeeService.deleteEmployee(email);
			response.put("success", "El empleado con email " + email + " fue borrado con éxito!");
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.OK);
		} catch (IdEmployeeNullException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.BAD_REQUEST);
		} catch (NotExistsEmployeeException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			response.put("error", "Ha ocurrido un error al intentar borrar el empleado");
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Método para obtener un empleado
	 * @param email: Correo del empleado que se quiere obtener
	 * @return empleado o mensaje de error
	 */
	@GetMapping("/{email}")
	public ResponseEntity<?> getEmployee(@PathVariable String email) {
		Map<String, String> response = new HashMap<String, String>();
		try {
			return new ResponseEntity<Employee>(employeeService.getEmployee(email), HttpStatus.OK);
		} catch (IdEmployeeNullException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.BAD_REQUEST);
		} catch (NotExistsEmployeeException e) {
			response.put("error", e.getMessage());
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			response.put("error", "Ha ocurrido un error al intentar obtener el empleado");
			return new ResponseEntity<Map<String, String>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Método para filtrar empleados 
	 * @param employee: Empleado que contiene la información que se quiere filtrar en sus atributos
	 * @param page: Número de página que se quiere obtener, se obtienen de 10 en 10
	 * @return lista de empleados filtrados
	 */
	@PostMapping("/filter/page/{page}")
	public Page<Employee> findEmployeesFilter(@RequestBody Employee employee, @PathVariable Integer page) {
		return employeeService.filterEmployees(employee, PageRequest.of(page, 10));
	}
}
