package com.example.reto.exceptions;

public class ExistIdentficationTypeAndNumberException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Excepcion que indica un error cuando se quiere registrar un empleado con una identificacion y tipo de identificacion que ya existen en la base de datos
	 */
	public ExistIdentficationTypeAndNumberException() {
		super("El tipo de identificación y el número de identificación que intenta ingresar ya se encuentra registrado");
	}

}
