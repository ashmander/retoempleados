package com.example.reto.exceptions;

public class AdmissionDateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Excepción que indica un error en la fecha de ingreso
	 */
	public AdmissionDateException() {
		super("La fecha de ingreso no puede ser menor a un mes o superior a la fecha actual");
	}
}
