package com.example.reto.exceptions;

public class IdEmployeeNullException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Excepcion que indica que el id del empleado es nulo
	 */
	public IdEmployeeNullException() {
		super("El id y el empleado no puede ser nulo");
	}
}
