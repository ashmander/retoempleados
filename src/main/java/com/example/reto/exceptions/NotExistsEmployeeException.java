package com.example.reto.exceptions;

public class NotExistsEmployeeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Excepcion que indica un error cuando se quiere actualizar un empleado si el correo dado no se encuentra registrado en la base de datos
	 * @param email
	 */
	public NotExistsEmployeeException(String email) {
		super("El empleado con email "+email+" no existe en la base de datos");
	}
}
