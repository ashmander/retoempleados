package com.example.reto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.reto.model.Traceability;

public interface TraceabilityRepository extends JpaRepository<Traceability, Long> {

}
