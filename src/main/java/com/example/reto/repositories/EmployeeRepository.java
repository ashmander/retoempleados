package com.example.reto.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.reto.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

	public Employee findByEmail(String email);
	
	public List<Employee> findByEmailContainingOrderByEmailAsc(String email);
	
	public Employee findByIdentificationTypeIdAndIdentificationNumber(Long identificationTypeId, String identificationNumber);
	
	@Query(value = "select * from employees where "
			+ "(?1 is null or email like %?1%)"
			+ " and "
			+ "(?2 is null or identification_type_id = ?2)"
			+ " and "
			+ "(?3 is null or first_name like %?3%)"
			+ " and "
			+ "(?4 is null or other_names like %?4%)"
			+ " and "
			+ "(?5 is null or first_last_name like %?5%)"
			+ " and "
			+ "(?6 is null or second_last_name like %?6%)"
			+ " and "
			+ "(?7 is null or identification_number like %?7%)"
			+ " and "
			+ "(?8 is null or state like %?8%)"
			+ " and "
			+ "(?9 is null or country_id = ?9)"
			, nativeQuery = true)
	public Page<Employee> findByEmailContainingAndIdentificationTypeId(
			String email, 
			Long identificationTypeId,
			String firstName,
			String otherNames,
			String firstLastName,
			String secondLastName,
			String identificationNumber,
			String state,
			Long  countryId,
			Pageable pageable);
	
}
