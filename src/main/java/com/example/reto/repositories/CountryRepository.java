package com.example.reto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.reto.model.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

}
