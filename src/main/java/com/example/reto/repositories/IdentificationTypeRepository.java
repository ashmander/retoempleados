package com.example.reto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.reto.model.IdentificationType;

public interface IdentificationTypeRepository extends JpaRepository<IdentificationType, Long> {

}
