package com.example.reto.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.reto.model.Area;

public interface AreaRepository extends JpaRepository<Area, Long>{

}
